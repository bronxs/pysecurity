# PiSecurity

IOT's project with RasberryPi, Python and OpenCV.

Security camera detecting human capturing by camera using 2 method algorithm; 
- Hog OpenCV algorithm.
- Haar cascade OpenCV algorithm.

### Push an existing folder
- cd existing_folder
- git init
- git remote add origin https://gitlab.com/bronxs/pysecurity.git
- git add .
- git commit -m "Initial commit"
- git push -u origin master